/*
 * u_uart.c
 *
 * Created: 2/12/2019 2:08:36 PM
 *  Author: twang
 */ 

/**
 * \brief Configure UART console.
 */
/** UART module for debug. */
#include "asf.h"


///* Max size of UART buffer. */
//#define MAIN_CHAT_BUFFER_SIZE 10//wt 64

static struct usart_module cdc_uart_module;

///** Written size of UART buffer. */
//static int uart_buffer_written = 0;
//
///** A buffer of character from the serial. */
//static uint16_t uart_ch_buffer;
//
///** UART buffer. */
//char uart_buffer[MAIN_CHAT_BUFFER_SIZE];


//static void uart_callback(const struct usart_module *const module)
//{
	///* If input string is bigger than buffer size limit, ignore the excess part. */
	//if (uart_buffer_written < MAIN_CHAT_BUFFER_SIZE) {
		//uart_buffer[uart_buffer_written++] = uart_ch_buffer & 0xFF;
	//}
//}

/**
 * \brief Checking the USART buffer.
 *
 * Finding the new line character(\n or \r\n) in the USART buffer.
 * If buffer was overflowed, Sending the buffer.
 */
//static void check_usart_buffer(char *topic)
//{
	//int i;
	//static volatile int j;
	//
//
	///* Publish the input string when newline was received or input string is bigger than buffer size limit. */
	//if (uart_buffer_written >= MAIN_CHAT_BUFFER_SIZE) {
		////wt mqtt_publish(&mqtt_inst, topic, uart_buffer, MAIN_CHAT_BUFFER_SIZE, 0, 0);
		//uart_buffer_written = 0;
	//} else {
		//for (i = 0; i < uart_buffer_written; i++) {
			///* Find newline character ('\n' or '\r\n') and publish the previous string . */
			//if (uart_buffer[i] == '\n') {
				//j++; if(j > 9) j = 0;
				//uart_buffer[0] = j + '0';
				////wt mqtt_publish(&mqtt_inst, topic, uart_buffer, (i > 0 && uart_buffer[i - 1] == '\r') ? i - 1 : i, 0, 0); // Qos changed to 1 NK
				///* Move remain data to start of the buffer. */
				//if (uart_buffer_written > i + 1) {
					//memmove(uart_buffer, uart_buffer + i + 1, uart_buffer_written - i - 1);
					//uart_buffer_written = uart_buffer_written - i - 1;
				//} else {
					//uart_buffer_written = 0;
				//}
//
				//break;
			//}
		//}
	//}
//}

void configure_console(void)
{
	struct usart_config usart_conf;

	usart_get_config_defaults(&usart_conf);
	usart_conf.mux_setting = EDBG_CDC_SERCOM_MUX_SETTING;
	usart_conf.pinmux_pad0 = EDBG_CDC_SERCOM_PINMUX_PAD0;
	usart_conf.pinmux_pad1 = EDBG_CDC_SERCOM_PINMUX_PAD1;
	usart_conf.pinmux_pad2 = EDBG_CDC_SERCOM_PINMUX_PAD2;
	usart_conf.pinmux_pad3 = EDBG_CDC_SERCOM_PINMUX_PAD3;
	usart_conf.baudrate    = 115200;

	stdio_serial_init(&cdc_uart_module, EDBG_CDC_MODULE, &usart_conf);
	/* Register USART callback for receiving user input. */
	//usart_register_callback(&cdc_uart_module, (usart_callback_t)uart_callback, USART_CALLBACK_BUFFER_RECEIVED);
	usart_enable(&cdc_uart_module);
}
