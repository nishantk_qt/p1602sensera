/*
 * u_parse.c
 *
 * Created: 2/12/2019 1:43:48 PM
 *  Author: twang
 */ 
#include "asf.h"
#include "main.h"
#include "u_rtc.h"
#include "u_mqtt.h"
/************************************************************************/
/*
Function: Pulse width parse
Parameter: None
Return: bool, 0x0: Flush, 1: Leak, 2: Overflow, 3: periodically upload
*/
/************************************************************************/


void pulse_width_parse(void){
	if(water_state.report){
		water_state.leak = 2;
		water_state.flush = 2;
		water_state.report = 0;
		//water_state.send = 1;
		//pulse.width = 0;
		return;
	}
	
	if (!water_state.send) return;
	
	if(pulse.width < 150){ //noise
		water_state.noise = 1;
		//buzzer_ctrl.count = 0;
	}
	else if(pulse.width <= 10000) {//LEAK_TIME){ //leakage time less than 10 seconds
		water_state.leak = 1;
		water_state.flush = 0;
		pulse.total_leak +=1;
		//buzzer_ctrl.count = 2;
		printf("Leak, %d , %d ms\r\n", pulse.width);
		//return 0;
	}
	else if(pulse.width < 30000) {//FLUSH_TIME) { //less than 0.5 minutes
		water_state.leak = 0;
		water_state.flush = 1;
		pulse.total_flush +=1;
		//buzzer_ctrl.count = 4;
		printf("Flush, %d ms\r\n", pulse.width);
		//return 1;
	}
	else {
		water_state.leak = 1;
		water_state.flush = 1;
		//buzzer_ctrl.count = 10;

		printf("Overflow!\r\n");
		//return 2;
	}
}

void shift_right(uint8_t arr[], uint8_t start, uint8_t end){ //circular shift, e.g. 41xx -> xx41
	uint8_t k = 0;
	for(k = start; k <= end; k++){ //get first 'x' position
		if(arr[k] == 'x') break;
	}
	for(uint8_t j = 0; j <= end - k; j++){ //shift 'end - k' = number of 'x' times
		uint8_t temp = arr[end];
		for(uint8_t i = end-1; i >= start; i--){
			arr[i+1] = arr[i];
		}
		arr[start] = temp;
	}
}


/************************************************************************/
/*
Function: Pulse width package
Parameter: None
Return: None
*/
/************************************************************************/

void pulse_width_package(void){
	struct rtc_calendar_time time;
	//! [rtc_module_instance]
	//struct rtc_module rtc_instance;
	char topic[7] = "Sensera";
	uint8_t temp[44] = "Sensera001xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
	uint8_t unit_id_addr = 7;
	uint8_t package_id_addr = 10;
	uint8_t timestamp_addr = 14;
	uint8_t flush_flg_addr = 28;
	uint8_t leak_flg_addr = 29;
	uint8_t flush_time_addr = 30;
	uint8_t leak_time_addr = 38;
	
	pulse.package_ID++;
	//rtc_calendar_get_time(&rtc_instance, &time);
	time = get_time();
	sprintf(&temp[package_id_addr], "%u", pulse.package_ID);
	sprintf(&temp[timestamp_addr], "%u", time.second);
	sprintf(&temp[timestamp_addr+2], "%u", time.minute);
	sprintf(&temp[timestamp_addr+4], "%u", time.hour);
	sprintf(&temp[timestamp_addr+6], "%u", time.day);
	sprintf(&temp[timestamp_addr+8], "%u", time.month);
	sprintf(&temp[timestamp_addr+10], "%u", time.year);
	sprintf(&temp[flush_flg_addr], "%u", water_state.flush);
	sprintf(&temp[leak_flg_addr], "%u", water_state.leak);
	if(water_state.flush==2 && water_state.leak==2){ //periodical report
		sprintf(&temp[flush_time_addr], "%u", pulse.total_flush);
		sprintf(&temp[leak_time_addr], "%u", pulse.total_leak);
		pulse.total_flush = 0;
		pulse.total_leak = 0;
	}
	else{
		sprintf(&temp[flush_time_addr], "%u", pulse.width/1000);
		if(pulse.width < 99999) sprintf(&temp[leak_time_addr], "%u", pulse.width); //can only display 5 digits
		else sprintf(&temp[leak_time_addr], "%u", 0);
	}
	
	
	
	for(uint8_t i = 0; i < 44; i++){//used to replace space added by sprintf
		if(temp[i] == '\0'){
			temp[i] = 'x';
		}
	}
	shift_right(temp, package_id_addr, package_id_addr+3);
	shift_right(temp, timestamp_addr, timestamp_addr+1);
	shift_right(temp, timestamp_addr+2, timestamp_addr+3);
	shift_right(temp, timestamp_addr+4, timestamp_addr+5);
	shift_right(temp, timestamp_addr+6, timestamp_addr+7);
	shift_right(temp, timestamp_addr+8, timestamp_addr+9);
	shift_right(temp, timestamp_addr+10, timestamp_addr+13);
	shift_right(temp, flush_time_addr, flush_time_addr+7);
	shift_right(temp, leak_time_addr, leak_time_addr+4);
	
	for(uint8_t i = 0; i < 44; i++){//
		if(temp[i] == 'x'){
			temp[i] = '0';
		}
	}
	temp[43] = '\0'; //stop print here
	
	//printf("hour = %d, minute = %d, second = %d\r\n", time.hour, time.minute, time.second);
	printf("%s\r\n", temp);
	//if(!water_state.noise)	sendto(tx_socket, &temp, sizeof(temp)-1, 0, (struct sockaddr *)&addr, sizeof(addr));
	//return water_state.noise;
	
	//char topic[strlen(MAIN_CHAT_TOPIC) + MAIN_CHAT_USER_NAME_SIZE + 1];
	u_publish(topic, temp, strlen(temp));
}

/************************************************************************/
/*
Function: Upload package
Parameter: None
Return: None
*/
/************************************************************************/
void upload_data(void) {
	bool upload = false;
	
	pulse_width_parse();
	/*if (water_state.report) {  //periodically report to server every 2 mins
		water_state.report = 0;
		upload = true;
	}*/
	if (water_state.flush != 0 || water_state.leak !=0) {
		upload = true;
	}
	if (upload) {
		pulse_width_package();
		water_state.flush = 0;
		water_state.leak = 0;
		//water_state.send = 0;
		pulse.width = 0;
	}
	//return upload;
}