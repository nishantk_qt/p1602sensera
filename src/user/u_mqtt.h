/*
 * u_mqtt.h
 *
 * Created: 2/13/2019 10:27:51 AM
 *  Author: twang
 */ 


#ifndef U_MQTT_H_
#define U_MQTT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define TEST 1
/* Max size of MQTT buffer. */
#define MAIN_MQTT_BUFFER_SIZE 128

/* Limitation of user name. */
#define MAIN_CHAT_USER_NAME_SIZE 64

/* Chat MQTT topic. */
#define MAIN_CHAT_TOPIC "atmel/sample/chat_demo/"

/*
 * A MQTT broker server which was connected.
 * test.mosquitto.org is public MQTT broker.
 */
//static const char main_mqtt_broker[] = "test.mosquitto.org";
static const char main_mqtt_broker[] = "broker.hivemq.com";
//static const char main_mqtt_broker[] = "m2m.eclipse.org";
//static const char main_mqtt_broker[] = "192.168.1.12";
//static const char main_mqtt_broker[] = "test.mosca.io";

#if TEST == 0
/** Wi-Fi Settings */
//static const char main_mqtt_broker[] = "192.168.1.6";
//#define MAIN_WLAN_SSID        "Quiretech" /* < Destination SSID */
//#define MAIN_WLAN_AUTH        M2M_WIFI_SEC_WPA_PSK /* < Security manner */
//#define MAIN_WLAN_PSK         "connecttoquire123" /* < Password for Destination SSID */
#define MAIN_WLAN_SSID              "Sensera"//"FileHubPlus-E12E" /* < Destination SSID */
#define MAIN_WLAN_AUTH              M2M_WIFI_SEC_WPA_PSK /* < Security manner */
#define MAIN_WLAN_PSK               "senseratest"//"11111111" /* < Password for Destination SSID */

#else
/** Wi-Fi Settings */
//static const char main_mqtt_broker[] = "10.10.10.4";
#define MAIN_WLAN_SSID        "Quiretech"//"Sensera" /* < Destination SSID */
#define MAIN_WLAN_AUTH        M2M_WIFI_SEC_WPA_PSK /* < Security manner */
#define MAIN_WLAN_PSK         "connecttoquire123"//"quiretech16" /* < Password for Destination SSID */
#endif

void u_mqtt_init(void);
void u_sw_timer_task(void);
void u_m2m_wifi_handle_events(void);
void u_publish(char *topic, char *data, uint8_t data_len);

#ifdef __cplusplus
}
#endif

#endif /* U_MQTT_H_ */