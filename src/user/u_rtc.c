/*
 * u_rtc.c
 *
 * Created: 2/12/2019 2:07:30 PM
 *  Author: twang
 */ 
#include "asf.h"

static struct rtc_calendar_time time;

//! [rtc_module_instance]
static struct rtc_module rtc_instance;
//! [rtc_module_instance]

//! [alarm_struct]
struct rtc_calendar_alarm_time alarm;

void configure_rtc_calendar(void)
{
	/* Initialize RTC in calendar mode. */
	//! [init_conf]
	struct rtc_calendar_config config_rtc_calendar;
	rtc_calendar_get_config_defaults(&config_rtc_calendar);
	//! [init_conf]

	//! [time_struct]

	alarm.time.year      = 2018;
	alarm.time.month     = 7;
	alarm.time.day       = 2;
	alarm.time.hour      = 15;
	alarm.time.minute    = 25;
	alarm.time.second    = 0;
	//! [time_struct]

	//! [set_config]
	config_rtc_calendar.clock_24h = true;
	config_rtc_calendar.alarm[0].time = alarm.time;
	config_rtc_calendar.alarm[0].mask = RTC_CALENDAR_ALARM_MASK_YEAR;
	//! [set_config]

	//! [init_rtc]
	rtc_calendar_init(&rtc_instance, RTC, &config_rtc_calendar);
	//! [init_rtc]

	//! [enable]
	rtc_calendar_enable(&rtc_instance);
	//! [enable]
}

//void time_parse_set(void){ //parse the synchronize time and set the RTC
	//
	////time.year   = (gau8SocketTestBuffer[0]-'0')*1000
	////+ (gau8SocketTestBuffer[1]-'0')*100
	////+ (gau8SocketTestBuffer[2]-'0')*10
	////+ (gau8SocketTestBuffer[3]-'0');//2012;
	////time.month  = (gau8SocketTestBuffer[5]-'0')*10 + (gau8SocketTestBuffer[6]-'0');//12;
	////time.day    = (gau8SocketTestBuffer[8]-'0')*10 + (gau8SocketTestBuffer[9]-'0');//30;
	////time.hour   = (gau8SocketTestBuffer[11]-'0')*10 + (gau8SocketTestBuffer[12]-'0');//22;
	////time.minute = (gau8SocketTestBuffer[14]-'0')*10 + (gau8SocketTestBuffer[15]-'0');//55;
	////time.second = (gau8SocketTestBuffer[17]-'0')*10 + (gau8SocketTestBuffer[18]-'0');//59;
	//
	////rtc_calendar_set_time(&rtc_instance, &time); //set time
//}

void rtc_init(void) {
	time.year   = 2012;
	time.month  = 12;
	time.day    = 30;
	time.hour   = 22;
	time.minute = 55;
	time.second = 59;
	
	configure_rtc_calendar();
	rtc_calendar_set_time(&rtc_instance, &time); //set time
}

struct rtc_calendar_time get_time(void) {
	rtc_calendar_get_time(&rtc_instance, &time);
	return time;
}