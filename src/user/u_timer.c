/*
 * u_timer.c
 *
 * Created: 2/12/2019 1:42:16 PM
 *  Author: twang
 */ 

#include "asf.h"
#include "main.h"

#define WAVE_INPUT BUTTON_0_EIC_PIN //wt
#define TC_COUNT_VALUE 17535  //41535
#define TC_WAKE_VALUE  62335  //100 sec 64575//30sec //
#define Buzzer PIN_PA04       //EXT1_PIN_3
#define INT_MAX_32 4294967295

static struct tc_module tc_instance;
static struct tc_module tc_instance_wk;



/** TC Callback function.
 */
static void tc_callback_to_counter( //1kHz
		struct tc_module *const module_inst)
{
	bool pin_state = port_pin_get_input_level(WAVE_INPUT);
	buzzer_ctrl.timer_cnt++;
	if (buzzer_ctrl.timer_cnt >= 500) {
		buzzer_ctrl.timer_cnt = 0;
		buzzer_ctrl.time_out = 1;
	}
	//port_pin_toggle_output_level(LED_0_PIN);
	if(!pin_state){ //check input level
		//pin_pre_state = pin_state;
		pulse.cnt_ctrl = 1;
		pulse.count++;
		if(pulse.count == INT_MAX_32) pulse.count = 0;
		if(pulse.count == 30000) { //OVERFLOW_TIME){
			water_state.send = 2; //send warning immediately
			pulse.width = pulse.count;
			pulse.count = 0;
			
			struct port_config port_confg;
			port_get_config_defaults(&port_confg);
			port_confg.direction = PORT_PIN_DIR_OUTPUT;
			port_pin_set_config(Buzzer, &port_confg);
			port_pin_set_output_level(Buzzer, true);
		}
	}
	else{
		//sample_count++;
		if(pulse.cnt_ctrl){// pulse ended
			pulse.cnt_ctrl = 0;
			if (water_state.send == 2) {
				water_state.send = 0; //if the previous sending is for overflow, don't send this one
			}
			else {
				water_state.send = 1; //data is ready to be sent
			}
			if (water_state.send) {
				pulse.width = pulse.count;
			}
			//printf("Pulse Width = %d\r\n", pulse.count);
		}
		pulse.count = 0;
	}
	
	tc_set_count_value(module_inst,TC_COUNT_VALUE);
}

/** Configures  TC function with the  driver.
 */
static void configure_tc(void)
{
	struct tc_config config_tc;

	tc_get_config_defaults(&config_tc);
	config_tc.counter_size    = TC_COUNTER_SIZE_16BIT;
	config_tc.counter_16_bit.value = TC_COUNT_VALUE;
	//config_tc.counter_size    = TC_COUNTER_SIZE_32BIT;
	//config_tc.counter_32_bit.value = TC_COUNT_VALUE;

	tc_init(&tc_instance, CONF_TC_INSTANCE, &config_tc);
	tc_enable(&tc_instance);
}

/** Registers TC callback function with the  driver.
 */
static void configure_tc_callbacks(void)
{
	tc_register_callback(
			&tc_instance,
			tc_callback_to_counter,
			TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&tc_instance, TC_CALLBACK_OVERFLOW);
}

static void tc_callback_to_wk(struct tc_module *const module_inst){ //timer for wake up
	water_state.report = 1;
	port_pin_toggle_output_level(LED_0_PIN);
	tc_set_count_value(module_inst,TC_WAKE_VALUE);
}

/** Configures  TC function with the  driver.
 */
static void configure_tc_wk(void) //timer for wake up
{
	struct tc_config config_tc;

	tc_get_config_defaults(&config_tc);
	config_tc.counter_size    = TC_COUNTER_SIZE_16BIT;
	config_tc.counter_16_bit.value = TC_WAKE_VALUE;
	config_tc.clock_source        = GCLK_GENERATOR_1;
	config_tc.clock_prescaler     = TC_CLOCK_PRESCALER_DIV1024;
	config_tc.run_in_standby      = true;

	tc_init(&tc_instance_wk, TC_WAKE_UP, &config_tc);
	tc_enable(&tc_instance_wk);
}

/** Registers TC callback function with the  driver.
 */
static void configure_tc_callbacks_wk(void)
{
	tc_register_callback(
			&tc_instance_wk,
			tc_callback_to_wk,
			TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&tc_instance_wk, TC_CALLBACK_OVERFLOW);
}


void u_tc_init(void) {
		
	configure_tc(); //config timer
	
	/*Configures TC callback*/
	configure_tc_callbacks();
	
	/*Configures the External Interrupt callback*/
	configure_tc_wk(); //config timer
	
	/*Configures TC callback*/
	configure_tc_callbacks_wk();
}

