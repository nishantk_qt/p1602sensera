/*
 * u_rtc.h
 *
 * Created: 2/12/2019 2:07:51 PM
 *  Author: twang
 */ 


#ifndef U_RTC_H_
#define U_RTC_H_

void rtc_init(void);
struct rtc_calendar_time get_time(void);


#endif /* U_RTC_H_ */