/**
 * \file
 *
 * \brief MAIN configuration.
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED


#ifdef __cplusplus
extern "C" {
#endif

//#define LEAK_TIME 10000
//#define FLUSH_TIME 30000//100000
//#define OVERFLOW_TIME 30000//300000
typedef struct s_pulse {
	bool cnt_ctrl;     //count the samples only when watering
	uint32_t count;    //count how many times the timer get triggered
	uint32_t width;    //how long the water runs in an event
	uint16_t total_leak;  //how many times leakage a day
	uint16_t total_flush; //times of flush
	uint16_t package_ID;  //
}t_pulse;


typedef struct s_state {
	uint8_t send;          //sending data to server
	//bool button;        //indicate whether
	uint8_t flush;         //The toilet is flushing.
	uint8_t leak;          //It's leaking.
	uint8_t report;        //report periodically
	bool run;           //the water is running
	bool noise;         //if the interrupt duration is less than 150 ms
	bool time_sync;     //synchronize time, 0: not synchronize, 1: received time from server, 2: synchronize
	//uint8_t bind_flg;      //if the socket is bind to local IP
} t_state;

typedef struct {
	uint8_t count; // times of bip
	uint8_t time_out; // enable the buzzer every 0.5 seconds
	uint32_t timer_cnt; //bip or not
	bool en_output; //enable output
}t_buzzer_ctrl;

extern volatile t_state water_state;
extern volatile t_pulse pulse;
extern volatile t_buzzer_ctrl buzzer_ctrl;


#ifdef __cplusplus
}
#endif

#endif /* MAIN_H_INCLUDED */
