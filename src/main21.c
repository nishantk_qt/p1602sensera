/**
 *
 * \file
 *
 * \brief WINC1500 MQTT chat example.
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

/** \mainpage
 * \section intro Introduction
 * This example demonstrates the use of the WINC1500 with the SAMD21 Xplained Pro
 * board to implement an MQTT based chat.
 * It uses the following hardware:
 * - the SAMD21 Xplained Pro.
 * - the WINC1500 on EXT1.
 *
 * \section files Main Files
 * - main.c : Initialize the WINC1500, connect to MQTT broker and chat with the other devices.
 * - mqtt.h : Implementation of MQTT 3.1
 *
 * \section usage Usage
 * -# Configure below code in the main.h for AP information to be connected.
 * \code
 *    #define MAIN_WLAN_SSID         "DEMO_AP"
 *    #define MAIN_WLAN_AUTH         M2M_WIFI_SEC_WPA_PSK
 *    #define MAIN_WLAN_PSK          "12345678"
 * \endcode
 * -# Build the program and download it into the board.
 * -# On the computer, open and configure a terminal application as the follows.
 * \code
 *    Baud Rate : 115200
 *    Data : 8bit
 *    Parity bit : none
 *    Stop bit : 1bit
 *    Flow control : none
 *    Line-Ending style : LF or CR+LF
 * \endcode
 * -# Start the application.
 * -# In the terminal window, First of all enter the user name through the terminal window.
 * -# And after the text of the following is displayed, please enjoy the chat.
 * -# Initialization operations takes a few minutes according to the network environment.
 * \code
 *    Preparation of the chat has been completed.
 * \endcode
 *
 * \section known_issue Known Issue
 * -# The user name cannot contain space (' ').
 * -# Cannot send more than 128 bytes.
 * -# User name must be unique. If someone uses the same user name, Which one will be disconnected.
 * -# USART interface has not error detection procedure. So sometimes serial input is broken.
 *
 * \section compinfo Compilation Information
 * This software was written for the GNU GCC compiler using Atmel Studio 6.2
 * Other compilers may or may not work.
 *
 * \section contactinfo Contact Information
 * For further information, visit
 * <A href="http://www.atmel.com">Atmel</A>.\n
 */

#include "asf.h"
#include "main.h"
#include "u_uart.h"
#include "u_interrupt.h"
#include "u_rtc.h"
#include "u_timer.h"
#include "u_mqtt.h"
#include "u_parse.h"
#include "driver/include/m2m_wifi.h"

/* Application instruction phrase. */
#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- WINC1500 Wi-Fi MQTT chat example --"STRING_EOL \
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

#define USER_LED PIN_PA11
#define STATUS_LED_R PIN_PB03
#define STATUS_LED_G PIN_PA02
#define BUZZER PIN_PA03
//#define timer_test 480000 * 3

void buz_alarm();

/* Watering states */
volatile t_state water_state = {
	.send = 0,
	.flush = 0,
	.leak = 0,
	.report = 0,
	.run = 0,
	.noise = 0,
	.time_sync = 0
};

/* Watering event */
volatile t_pulse pulse = {
	.cnt_ctrl = 0,
	.count = 0,
	.width = 0
};

volatile t_buzzer_ctrl buzzer_ctrl = {
	.count = 0,
	.time_out = 0,
	.timer_cnt = 0,
	.en_output = false
};

void buzzer_init(void) {
	struct port_config config_port_pin;
	port_get_config_defaults(&config_port_pin);
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(BUZZER, &config_port_pin);
	port_pin_set_output_level(BUZZER, false);
}

void led_init(void) {
	struct port_config config_port_pin;
	port_get_config_defaults(&config_port_pin);
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(STATUS_LED_R, &config_port_pin);
	port_pin_set_config(STATUS_LED_G, &config_port_pin);
	
	port_pin_set_output_level(USER_LED, LED_0_ACTIVE);
	port_pin_set_output_level(STATUS_LED_R, LED_0_INACTIVE);
	port_pin_set_output_level(STATUS_LED_G, LED_0_INACTIVE);
}

/**
 * \brief Main application function.
 *
 * Application entry point.
 *
 * \return program return value.
 */
int main(void)
{
	tstrWifiInitParam param;
	//int8_t ret;
	//char topic[strlen(MAIN_CHAT_TOPIC) + MAIN_CHAT_USER_NAME_SIZE + 1];
	//long long cnt = 0;
	
	/* Initialize the board. */
	system_init();

	/* Initialize the UART console. */
	configure_console();
	
	/* Initialize EIC */
	configure_extint();
	
	led_init();// initialize leds
	buzzer_init(); //initialize the buzzer

	/* Output example information */
	printf(STRING_HEADER);
	
	/* Initialize timers */
	u_tc_init();
	
	/* RTC initialization */
	rtc_init();

	/* Initialize the MQTT service. */
	//configure_mqtt();
	u_mqtt_init();
	/* Initialize the Timer. */
	
	while (1) {
		/* Handle pending events from network controller. */
		m2m_wifi_handle_events(NULL);
		//u_m2m_wifi_handle_events();
		/* Try to read user input from USART. */
		//wt usart_read_job(&cdc_uart_module, &uart_ch_buffer);
		/* Checks the timer timeout. */
		//sw_timer_task(&swt_module_inst);
		u_sw_timer_task();
		/* Checks the USART buffer. */
		//wt check_usart_buffer(topic);
		//delay_s(2);
		//printf("yes \r\n");
		//if (water_state.report == 1) {
		////if (cnt++ == timer_test) {
			//printf("eeeeee\r\n");
			//pulse.width = 38888;
			//water_state.report = 0;
			//water_state.send = 1;
			//cnt = 0;
		//}
		//u_publish();
		//buz_alarm();
		upload_data();	
	}
}


void buz_alarm(void) {
	/*if (buzzer_ctrl.count == 0) { //do not update buzzer if it is running
		if (pulse.width < 150) {
			;
		}
		else if(pulse.width <= LEAK_TIME){ //leakage time less than 10 seconds
			buzzer_ctrl.count = 2;
			//return 0;
		}
		else if(pulse.width < FLUSH_TIME) { //less than 0.5 minutes
			buzzer_ctrl.count = 4;
			//return 1;
		}
		else if (pulse.width >= OVERFLOW_TIME){
			buzzer_ctrl.count = 10;
			//return 2;
		}
		if (buzzer_ctrl.count > 0) {
			buzzer_ctrl.time_out = 1; //ring immediately
			printf("yes, %d, %d\r\n", pulse.width, buzzer_ctrl.count);
			//buzzer_ctrl.en_output = false;
		}
	}*/
	
	if (buzzer_ctrl.count != 0 && buzzer_ctrl.time_out == 1) {
		buzzer_ctrl.timer_cnt = 0;
		buzzer_ctrl.count--;
		buzzer_ctrl.time_out = 0;
		//printf("yes11111, %d\r\n", buzzer_ctrl.count);
		//buzzer_ctrl.en_output = !buzzer_ctrl.en_output;
		//port_pin_set_output_level(BUZZER, buzzer_ctrl.en_output);
		port_pin_toggle_output_level(BUZZER);
	}
}