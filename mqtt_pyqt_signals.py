from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import (QWidget, QLabel, QLineEdit, 
    QTextEdit, QGridLayout, QApplication)
from PyQt5.QtCore import *
from PyQt5.QtGui import QPalette,QColor

import paho.mqtt.client as mqtt


class MqttClient(QtCore.QObject):
    Disconnected = 0
    Connecting = 1
    Connected = 2

    MQTT_3_1 = mqtt.MQTTv31
    MQTT_3_1_1 = mqtt.MQTTv311

    connected = QtCore.pyqtSignal()
    disconnected = QtCore.pyqtSignal()

    stateChanged = QtCore.pyqtSignal(int)
    hostnameChanged = QtCore.pyqtSignal(str)
    portChanged = QtCore.pyqtSignal(int)
    keepAliveChanged = QtCore.pyqtSignal(int)
    cleanSessionChanged = QtCore.pyqtSignal(bool)
    protocolVersionChanged = QtCore.pyqtSignal(int)

    messageSignal = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(MqttClient, self).__init__(parent)

        self.m_hostname = ""
        self.m_port = 1883
        self.m_keepAlive = 60
        self.m_cleanSession = True
        self.m_protocolVersion = MqttClient.MQTT_3_1

        self.m_state = MqttClient.Disconnected

        self.m_client =  mqtt.Client(clean_session=self.m_cleanSession,
            protocol=self.protocolVersion)

        self.m_client.on_connect = self.on_connect
        self.m_client.on_message = self.on_message
        self.m_client.on_disconnect = self.on_disconnect


    @QtCore.pyqtProperty(int, notify=stateChanged)
    def state(self):
        return self.m_state

    @state.setter
    def state(self, state):
        if self.m_state == state: return
        self.m_state = state
        self.stateChanged.emit(state) 

    @QtCore.pyqtProperty(str, notify=hostnameChanged)
    def hostname(self):
        return self.m_hostname

    @hostname.setter
    def hostname(self, hostname):
        if self.m_hostname == hostname: return
        self.m_hostname = hostname
        self.hostnameChanged.emit(hostname)

    @QtCore.pyqtProperty(int, notify=portChanged)
    def port(self):
        return self.m_port

    @port.setter
    def port(self, port):
        if self.m_port == port: return
        self.m_port = port
        self.portChanged.emit(port)

    @QtCore.pyqtProperty(int, notify=keepAliveChanged)
    def keepAlive(self):
        return self.m_keepAlive

    @keepAlive.setter
    def keepAlive(self, keepAlive):
        if self.m_keepAlive == keepAlive: return
        self.m_keepAlive = keepAlive
        self.keepAliveChanged.emit(keepAlive)

    @QtCore.pyqtProperty(bool, notify=cleanSessionChanged)
    def cleanSession(self):
        return self.m_cleanSession

    @cleanSession.setter
    def cleanSession(self, cleanSession):
        if self.m_cleanSession == cleanSession: return
        self.m_cleanSession = cleanSession
        self.cleanSessionChanged.emit(cleanSession)

    @QtCore.pyqtProperty(int, notify=protocolVersionChanged)
    def protocolVersion(self):
        return self.m_protocolVersion

    @protocolVersion.setter
    def protocolVersion(self, protocolVersion):
        if self.m_protocolVersion == protocolVersion: return
        if protocolVersion in (MqttClient.MQTT_3_1, MQTT_3_1_1):
            self.m_protocolVersion = protocolVersion
            self.protocolVersionChanged.emit(protocolVersion)

    #################################################################
    @QtCore.pyqtSlot()
    def connectToHost(self):
        if self.m_hostname:
            self.m_client.connect(self.m_hostname, 
                port=self.port, 
                keepalive=self.keepAlive)

            self.state = MqttClient.Connecting
            self.m_client.loop_start()

    @QtCore.pyqtSlot()
    def disconnectFromHost(self):
        self.m_client.disconnect()

    def subscribe(self, path):
        if self.state == MqttClient.Connected:
            self.m_client.subscribe(path)
            #self.messageSignal.emit("220")
            print("Subcribe", path)

    #################################################################
    # callbacks
    def on_message(self, mqttc, obj, msg):
        mstr = msg.payload.decode("ascii")
        data = mstr
        print("on_message", mstr, obj, mqttc)
        self.messageSignal.emit(data)

    def on_connect(self, *args):
        print("on_connect", args)
        self.state = MqttClient.Connected
        self.connected.emit()

    def on_disconnect(self, *args):
        # print("on_disconnect", args)
        self.state = MqttClient.Disconnected
        self.disconnected.emit()


class Widget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Widget, self).__init__(parent)

        '''lay = QtWidgets.QVBoxLayout(self)
        self.lcd_number = QtWidgets.QLCDNumber()
        lay.addWidget(self.lcd_number)
        self.lcd_number_time = QtWidgets.QLCDNumber()
        lay.addWidget(self.lcd_number_time)'''
        self.messages = ''

        newfont_bold = QtGui.QFont("Times", 12, QtGui.QFont.Bold)
        newfont = QtGui.QFont("Times", 10)

        unit = QLabel('Unit')
        unit.setAlignment(Qt.AlignCenter)
        unit.setFont(newfont_bold)
        ID = QLabel('MES_ID')
        ID.setAlignment(Qt.AlignCenter)
        ID.setFont(newfont_bold)
        event = QLabel('Event')
        event.setAlignment(Qt.AlignCenter)
        event.setFont(newfont_bold)
        time = QLabel('Time(ms)')
        time.setAlignment(Qt.AlignCenter)
        time.setFont(newfont_bold)
        message = QLabel('Message')
        message.setFont(newfont_bold)

        self.unit_edit = QLineEdit()
        self.unit_edit.setAlignment(Qt.AlignCenter)
        self.unit_edit.setFont(newfont)
        self.ID_edit = QLineEdit()
        self.ID_edit.setAlignment(Qt.AlignCenter)
        self.ID_edit.setFont(newfont)
        self.event_edit = QLineEdit()
        self.event_edit.setAlignment(Qt.AlignCenter)
        self.event_edit.setFont(newfont)
        self.time_edit = QLineEdit()
        self.time_edit.setAlignment(Qt.AlignCenter)
        self.time_edit.setFont(newfont)
        self.mes_edit = QTextEdit()
        self.mes_edit.setFont(newfont)

        grid = QGridLayout()
        #grid.setSpacing(10)

        grid.addWidget(unit, 1, 0)
        grid.addWidget(self.unit_edit, 2, 0)
        
        grid.addWidget(ID, 1, 1)
        grid.addWidget(self.ID_edit, 2, 1)

        grid.addWidget(event, 1, 2)
        grid.addWidget(self.event_edit, 2, 2)

        grid.addWidget(time, 1, 3)
        #grid.addWidget(reviewEdit, 3, 1, 5, 1)
        grid.addWidget(self.time_edit, 2, 3)
        

        grid.addWidget(message, 3, 0)
        grid.addWidget(self.mes_edit, 4, 0, 6, 4)
        
        self.setLayout(grid) 
        
        self.setGeometry(300, 300, 500, 500)
        

        self.client = MqttClient(self)
        self.client.stateChanged.connect(self.on_stateChanged)
        self.client.messageSignal.connect(self.on_messageSignal)

        self.client.hostname = "broker.hivemq.com"
        self.client.connectToHost()

    @QtCore.pyqtSlot(int)
    def on_stateChanged(self, state):
        if state == MqttClient.Connected:
            print(state)
            self.client.subscribe("Sensera")

    @QtCore.pyqtSlot(str)
    def on_messageSignal(self, msg):
        try:
            val = msg[7:10]
            self.unit_edit.setText(val)
            val = msg[10:14]
            self.ID_edit.setText(val)
            val = int(msg[-5:])
            self.time_edit.setText(str(val))
            val = msg[28:30]
            if val == '01':
                self.event_edit.setText('Leak')
            elif val == '10':
                self.event_edit.setText('Flush')
            elif val == '11':
                self.event_edit.setText('Overflow')
            else:
                self.event_edit.setText('Report')
            self.messages = msg + '\n' + self.messages
            self.mes_edit.setText(self.messages)
        except ValueError:
            print("error: Not is number")
        pass


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    w = Widget()
    w.setWindowTitle('Sensera')
    
    w.show()
    sys.exit(app.exec_())
